package ru.nsu.ojp2020;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Objects;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Provide path to directory with class files");
            return;
        }

        MyClassLoader loader = new MyClassLoader();
        String directoryPath = args[0];
        File dir = new File(directoryPath);
        if (!dir.isDirectory()) {
            System.out.println(directoryPath + " is not a directory");
            return;
        }

        for (File file : Objects.requireNonNull(dir.listFiles())) {
            System.out.println(file.getName());
            System.out.println(file.getAbsolutePath());
            final Class<?> aClass = loader.findClass(file.getAbsolutePath());

            try {
                final Method method = aClass.getDeclaredMethod("getSecurityMessage");
                System.out.println("Class name: " + aClass.getCanonicalName());
                System.out.println("Class package: " + aClass.getPackageName());
                System.out.println("This Class package: " + Main.class.getPackageName());

                final Object instance = aClass.getDeclaredConstructor().newInstance();
                method.trySetAccessible();
                final Object result = method.invoke(instance);
                System.out.println("Result of the method is: " + result);
            } catch (NoSuchMethodException e) {
                System.out.println("Class " + aClass.getCanonicalName() +
                        " doesn't have method getSecurityMessage");
            }
        }
    }
}
