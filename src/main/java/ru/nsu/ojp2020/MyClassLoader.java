package ru.nsu.ojp2020;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyClassLoader extends ClassLoader {
    @Override
    protected Class<?> findClass(String name) throws ClassNotFoundException {
        File classFile = new File(name);
        if (!classFile.isFile()) {
            throw new ClassNotFoundException(name);
        }
        try (InputStream is = new BufferedInputStream(new FileInputStream(classFile))) {
            byte[] content = new byte[(int) classFile.length()];
            final int bytesRead = is.read(content);
            if (bytesRead == content.length) {
                final int i = name.lastIndexOf(File.separatorChar);
                final int j = name.lastIndexOf('.');
                String className = "ru.nsu.ojp2020." + name.substring(i + 1, j);

                return defineClass(className, content, 0, content.length);
            }
        } catch (IOException e) {
            e.printStackTrace();
            throw new ClassNotFoundException(name);
        }
        return super.findClass(name);
    }

}
